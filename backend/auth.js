// [Section] JSON Web Tokens
	const jwt = require("jsonwebtoken");

// [Section] Secret Keyword
	const secret = "baks";

// [Section] Token creation

	// Export a function named 'createAccessToken' as part of a module
	module.exports.createAccessToken = (user) => {
	    // Create an object 'data' containing user-specific information
	    const data = {
	        id: user._id,       // Extract and store the user's unique identifier
	        email: user.email,  // Extract and store the user's email address
	        isAdmin: user.isAdmin  // Extract and store whether the user is an admin or not
	    };

	    // Generate a JSON Web Token (JWT) using 'jsonwebtoken' library
	    // 'data': Payload data to be encoded in the JWT, containing user information
	    // 'secret': Secret key used to sign the JWT (should be kept secret and secure)
	    // {}: Additional options for JWT signing (empty in this case)
	    return jwt.sign(data, secret, {});
	};

// [Section] Token Verification

	/*
		-Analogy
		Receive the gift and open the lock to verify if the sender is legitimate and the gift was not tampered with
	*/

	module.exports.verify = (req, res, next) => {
		console.log(req.headers.authorization);

		// req.headers.authorization contains sensitive data and especially our token

		let token = req.headers.authorization;
		//This if statement will first check IF token variable contains undefined or a proper jwt. If it is undefined, we will check token's data type with typeof, then send a message to the client.

		if (typeof token === "undefined") {
			return res.send({ auth: "Failed. No Token" });
		} else {
			console.log(token);
			token = token.slice(7, token.length);
			console.log(token);

			// [Section] Token decryption
			/*
				- Analogy 
					Open the gift and get the content
			*/

			// Validate the token using the "verify" method decrypting the token usin the scecret code

			jwt.verify(token, secret, function(err, decedodedToken){
				if(err) {
					return res.send({
						auth: "Failed",
						message: err.message
					});
				} else {
					console.log(decodedToken); //Containes the data from our token

					req.user = decodedToken
					next();

				}
			})
		}
	};


// [Section] verifyAdmin will also be used a middleware.

	// Export a middleware function named 'verifyAdmin' as part of a module
	module.exports.verifyAdmin = (req, res, next) => {
	    // Check if the user associated with the request has an 'isAdmin' property set to true
	    if (req.user.isAdmin) {
	        // If the user is an admin, continue to the next middleware or route handler
	        next();
	    } else {
	        // If the user is not an admin, send a response indicating that the action is forbidden
	        return res.send({
	            auth: "Failed",      // Authentication status indicating failure
	            message: "Action Forbidden"  // Message indicating that the action is not allowed
	        });
	    }
	};

