//  Controller
const Product = require("../models/Product");
const User = require("../models/User");

// Add products

module.exports.createProduct = async (req, res) => {
  try {
    const { brand, model, size, description, price, stock, isActive } = req.body;
    const image = req.file ? req.file.filename : null;

    const newProduct = new Product({
      brand,
      model,
      size,
      description,
      price,
      stock,
      isActive,
      image,
    });

    await newProduct.save();
    res.status(201).json(newProduct);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Server error" });
  }
};