// [SECTION] Dependencies and Modules
	const User = require("../models/User");
	const Product = require("../models/Product");
	const bcrypt = require("bcrypt");
	const auth = require("../auth");

// User Registration

module.exports.registerUser = async (reqbody) => {
  try {

    // Check if the email is already in use
    const existingUser = await User.findOne({ email: reqbody.email });
    
    if (existingUser) {
       return res.status(404).send({ message: "Email is already in use" });
    }

    // If the email is not in use, proceed with registration
    let newUser = new User({
      firstName: reqbody.firstName,
      lastName: reqbody.lastName,
      email: reqbody.email,
      mobileNo: reqbody.mobileNo,
      password: bcrypt.hashSync(reqbody.password,10)
   	  
    });

    const user = await newUser.save();
    return true; // Return true upon successful user registration

  } catch (error) {
    console.error('Error registering user:', error);
    return false; // Return false if an error occurs during user registration
  }
};

//[SECTION] User authentication / Login

	module.exports.loginUser = (req, res) => {
		User.findOne({ email : req.body.email} ).then(result => {

			console.log(result);

			// User does not exist
			if(result == null){

				return res.send(false);

			// User exists
			} else {
				
				const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);
				// If the passwords match/result of the above code is true
				if (isPasswordCorrect) {
	
					return res.send({ access : auth.createAccessToken(result) })

				// Passwords do not match
				} else {

					return res.send(false);
				}
			}
		})
		.catch(err => res.send(err))
	};