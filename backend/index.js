// [Section] Dependencies and Modules
	const express = require("express");
	const mongoose = require("mongoose");
	const cors = require("cors");
	const multer = require("multer");
	// const userRoutes = require("./routes/user");
	const productRoutes = require("./routes/product");
	const userRoutes = require("./routes/user");

// [Section] Environment Setup
	const port = 4000;

// [Section] Server Setup
	const app = express();

	app.use(cors())
	app.use(express.json());
	app.use(express.urlencoded({ extended: true }));

// [Section] Database Connection
	mongoose.connect("mongodb+srv://admin2:IeM8DXZqPFEhtr8I@batch-297.h7rcrug.mongodb.net/Shoebox?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true
	});

	let db = mongoose.connection;
	db.on('error', console.error.bind(console, 'Connection error'));

	db.once('open', () => console.log('Connected to MongoDB Atlas.'));

// [Section] Backend Routes
	// app.use("/users",userRoutes);
	app.use("/users", productRoutes);
	app.use("/products", productRoutes);

//[SECTION] Server Gateway Response
// Check if the script is being run directly (as the main module)
if (require.main === module) {
    // If it is the main module, start the Express.js server and listen on a specified port
    app.listen(process.env.PORT || port, () => {
        // Log a message indicating that the API is online and listening on the specified port
        console.log(`API is now online on port ${process.env.PORT || port}`);
    });
}




module.exports = {app,mongoose};