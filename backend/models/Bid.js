const mongoose = require("mongoose");

const bidSchema = new mongoose.Schema({
    productId: {
        type: String,
        required: [true, "Product ID is required"],
    },
    productName: {
        type: String,
        required: [true, "Product name is required"],
    },
    productModel: {
        type: String,
        required: [true, "Product model is required"],
    },
    startingPrice: {
        type: Number,
        required: [true, "Starting price is required"],
    },
    currentBid: {
        type: Number,
        default: 0, // Initialize the current bid to 0
    },
    bids: [
        {
            userId: {
                type: String,
                required: [true, "User ID is required"],
            },
            bidAmount: {
                type: Number,
                required: [true, "Bid amount is required"],
            },
            bidTimestamp: {
                type: Date,
                default: new Date(),
            },
        },
    ],
    auctionEndsOn: {
        type: Date,
        required: [true, "Auction end date is required"],
    },
});

module.exports = mongoose.model("Bid", bidSchema);
