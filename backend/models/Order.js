// Dependencies

const mongoose = require("mongoose");

// Order model

const orderSchema = new mongoose.Schema({

	userId: {
		type: String,
		required: [true, "UserId is required"]
	},
	products: [
			{

				productId: {
					type: String,
					required: [true, "Product ID is required"]
				},
				productName: {
					type: String,
					required: [true, "Product name is required"]
				}
				productModel: {
					type: String,
					required: [true,"Product name is required"]
				},
				price: {
					type: Number,
					required: true
				},
				quantity: {
					type: Number,
					required: [true, "Quantity is required"]
				}


			}

		],
	totalAmount: {
		type: Number,
		required: [true, "Total amount is required"]
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	} 

})

// Model
module.exports = mongoose.model("Order", orderSchema);