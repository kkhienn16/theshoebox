// Dependencies and Modules
const express = require("express");
const productController = require("../controllers/product");
const auth = require("../auth");
const multer = require("multer");
const { verify, verifyAdmin } = auth;

// Routing component
const router = express.Router();

// [Section] Configure Multer for handling file uploads
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    // Define the destination folder where images will be stored
    cb(null, "uploads/"); // Create a "uploads" folder in your project directory
  },
  filename: function (req, file, cb) {
    // Define the filename for the uploaded image
    const uniqueSuffix = Date.now() + "-" + Math.round(Math.random() * 1e9);
    const extension = file.originalname.split('.').pop();
    cb(null, uniqueSuffix + "." + extension);
  },
});

const upload = multer({ storage: storage });

// Handle image upload and product creation
/*router.post("/create-product", upload.single("image"), productController.createProduct);*/









// Export Route System
module.exports = router;