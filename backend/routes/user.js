// [SECTION] Dependencies and Modules
	const express = require('express');
	const userController = require("../controllers/user");
	const auth = require("../auth");
	const { verify, verifyAdmin } = auth;

// [SECTION] Routing Component
	const router = express.Router();


// [SECTION] Route for user registration
router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(
		resultFromController => res.send(resultFromController
			))
});

// [SECTION] Route for user authentication
router.post("/login", userController.loginUser);